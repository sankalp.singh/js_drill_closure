const cacheFunction = require("../cacheFunction");

const funCall = cacheFunction(() => {
	return null;
});
console.log(funCall("arg1", "arg2", "arg1"));
console.log(funCall("arg3"));
console.log(funCall("arg3"));
console.log(funCall("arg4"));
