/* Should return a function that invokes `cb`.
     A cache (object) should be kept in closure scope.
     The cache should keep track of all arguments have been used to invoke this function.
     If the returned function is invoked with arguments that it has already seen
     then it should return the cached result and not invoke `cb` again.
     `cb` should only ever be invoked once for a given set of arguments.
    */
function cacheFunction(cb) {
	let cache = {};
	return function callBackInvoker(...arg) {
		// console.log(arg);
		if (typeof arg === "undefined" || arg.length === 0) {
			return cache;
		} else {
			for (let index = 0; index < arg.length; index++) {
				//if argument is inside the cache
				if (arg[index] in cache) {
					console.log(cache);
					// return cache;
				} else {
					// if argument is NOT in cache
					cache[arg[index]] = cb(arg);
					// console.log(cache);
				}
			}
		}
		return cache;
	};
}
module.exports = cacheFunction;
