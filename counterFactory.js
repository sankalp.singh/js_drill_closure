/*
     Return an object that has two methods called `increment` and `decrement`.
     `increment` should increment a counter variable in closure scope and return it.
     `decrement` should decrement the counter variable and return it.
*/

function counterFactory() {
	let count = 0;
	const obj = {
		increment: () => {
			count += 1;
			return count;
		},
		decrement: () => {
			count -= 1;
			return count;
		},
	};
	return obj;
}

module.exports = counterFactory;
