/*
     Should return a function that invokes `cb`.
     The returned function should only allow `cb` to be invoked `n` times.
     Returning null is acceptable if cb can't be returned
*/

function limitFunctionCallCount(cb, n) {
	return function callbackInvoker() {
		if (n > 0) {
			n--;
			const cbReturn = cb();
			if (cbReturn === null || typeof cb === undefined) {
				return null;
			} else {
				return cbReturn;
			}
			// console.log("Callback called!");
		} else {
			console.log("Callback Invoker has reached its limit");
		}
	};
}
module.exports = limitFunctionCallCount;
